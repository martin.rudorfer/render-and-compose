Hey there,

Thanks for your interest in this project and particularly for your considering to contribute!

You should know that this is my first open source project, so I am still learning the basics - from setting up the project, making a documentation (which is still missing), designing guidelines for contributing, finding the right git work flow etc.
I primarily develop this for my own research as phd student, and made it open source because.. why not? There are many things to learn. I am grateful for any feedback, comments or contributions, so please feel free to contact me (or just create an issue).

All the best,
Martin
