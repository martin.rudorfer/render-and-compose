import numpy as np
import trimesh
import pyrender
import cv2
import json
import os
import datetime
import configparser
from camera_pose_generators import RandomCameraPoseGenerator, IcoSphereCameraPoseGenerator


class ModelViewRenderer:
    """This class handles the rendering of model views."""

    def __init__(self, cfg):
        # read config data and set corresponding variables
        self.render_path = cfg['General']['renderings_directory']

        self.model_fn = os.path.join(cfg['Renderer']['model_dir'], cfg['Renderer']['model'])
        self.model_scale_factor = float(cfg['Renderer']['model_scale_factor'])

        self.image_size_x = int(cfg['Renderer']['image_size_x'])
        self.image_size_y = int(cfg['Renderer']['image_size_y'])
        self.crop_images = cfg['Renderer']['crop_images'].lower() in ['true', '1', 'yes', 'y']

        self.fx = float(cfg['Renderer']['fx'])
        self.fy = float(cfg['Renderer']['fy'])
        self.px = float(cfg['Renderer']['px'])
        self.py = float(cfg['Renderer']['py'])

        # configure the camera pose generator
        if cfg['Renderer']['camera_pose_generator'] == 'RandomCameraPoseGenerator':
            print('Using RandomCameraPoseGenerator with settings specified in config file.')
            self.camera_pose_generator = RandomCameraPoseGenerator(cfg['Renderer.RandomCameraPoseGenerator'])
        elif cfg['Renderer']['camera_pose_generator'] == 'IcoSphereCameraPoseGenerator':
            print('Using IcoSphereCameraPoseGenerator with settings specified in config file.')
            self.camera_pose_generator = IcoSphereCameraPoseGenerator(cfg['Renderer.IcoSphereCameraPoseGenerator'])
        else:
            print('WARNING: Unknown camera pose generator. Using Random with default settings.')
            self.camera_pose_generator = RandomCameraPoseGenerator()

    @staticmethod
    def load_model(scene, model_filename, pose=np.identity(4), scale=1):
        """Loads a model at specified pose into the scene.

        Args:
            scene: The scene to load the model to.
            model_filename: The filename (absolute/relative path) to model.
            pose (optional): Pose of the model. Defaults to identity.
            scale (optional): Factor to scale the model after loading, so that the model is in [m]
        """
        # check if file exists
        if not os.path.isfile(model_filename):
            print("ERROR: model file does not exist:", model_filename)
            return

        # load model and put it into a scene
        obj_trimesh = trimesh.load(model_filename)
        obj_trimesh.apply_scale(scale)
        mesh = pyrender.Mesh.from_trimesh(obj_trimesh)
        scene.add(mesh, pose=pose)
        print("ModelViewRenderer.load_model: added", model_filename, "to scene.")
        print(f"\tit has been scaled with factor {scale:f} and extents are now", obj_trimesh.extents)

    def visualize_camera_positions(self):
        scene = pyrender.Scene(ambient_light=np.asarray([0.8, 0.8, 0.8]))
        ModelViewRenderer.load_model(scene, self.model_fn, scale=self.model_scale_factor)

        # create some trivial mesh object to indicate the camera pose
        points = np.zeros((2, 3))
        length = 0.2  # [m]
        points[1, 0] = length
        point = pyrender.Mesh.from_points(points)

        # now let's add that trivial object for each pose
        i = 1
        self.camera_pose_generator.reset()
        print(f'visualizing {self.camera_pose_generator.number_of_poses:d} poses.')
        while self.camera_pose_generator.has_next_pose():
            # get next pose and set obj accordingly
            pose = self.camera_pose_generator.get_next_pose()
            scene.add(point, pose=pose)

            i += 1

        print('use [a] to toggle animation mode')
        print('use [r] to record a gif, or [s] to save the current view')
        print('use [i] to toggle axis display mode')
        pyrender.Viewer(scene, viewer_flags={'show_mesh_axes': True})

    def generate_renderings(self):
        #############################################
        # prepare the scene:
        # load model, configure camera and lights
        #############################################

        scene = pyrender.Scene()
        ModelViewRenderer.load_model(scene, self.model_fn, scale=self.model_scale_factor)

        # let's determine camera's znear and zfar as limits for rendering, with some safety margin (factor 2)
        lower_bound = self.camera_pose_generator.cam_distance_min * self.camera_pose_generator.distance_factor / 2
        upper_bound = self.camera_pose_generator.cam_distance_max * self.camera_pose_generator.distance_factor * 2

        camera = pyrender.IntrinsicsCamera(self.fx, self.fy, self.px, self.py, znear=lower_bound, zfar=upper_bound)
        cam_node = pyrender.Node(camera=camera)
        scene.add_node(cam_node)

        # set up the light -- a single spot light in the same spot as the camera
        light = pyrender.SpotLight(color=np.ones(3), intensity=3.0, innerConeAngle=np.pi/16.0)
        light_node = pyrender.Node(light=light)
        scene.add_node(light_node)

        ###################################
        # prepare ground truth file
        ###################################

        # save all info about rendering process, rendered files and object poses
        gt_file = os.path.join(self.render_path, 'ground_truth.txt')
        # settings of renderer
        ren_cfg = self.__dict__     # all variables of renderer
        ren_cfg = {key: ren_cfg[key] for key in ren_cfg if not key.startswith('_')}  # remove private variables
        # same for camera config
        gen_cfg = self.camera_pose_generator.__dict__
        gen_cfg = {key: gen_cfg[key] for key in gen_cfg if not key.startswith('_')}  # remove private variables
        # write to dict
        gt_dict = {
            'render_info': {
                'timestamp': datetime.datetime.now().isoformat(' ', 'seconds'),
                **ren_cfg,  # puts all key value pairs of ren_cfg here
                'camera_pose_generator': type(self.camera_pose_generator).__name__,
                'camera_pose_generator_config': gen_cfg
            },
            'image_labels': []
        }

        ###############################
        # start rendering loop
        ###############################

        # set up rendering settings
        r = pyrender.OffscreenRenderer(self.image_size_x, self.image_size_y)
        ext = '.png'
        i = 1

        self.camera_pose_generator.reset()
        while self.camera_pose_generator.has_next_pose():
            print(f'configuring and rendering scene number {i:04d}')

            # get next pose and set camera and light accordingly
            camera_pose = self.camera_pose_generator.get_next_pose()
            scene.set_pose(cam_node, pose=camera_pose)
            scene.set_pose(light_node, pose=camera_pose)

            # render the color and depth images
            # color is rgb, depth is mono float in [m]
            color, depth = r.render(scene)

            # create mask (where depth != 0 there is some object)
            mask = np.not_equal(depth, 0)
            mask = np.array(mask, dtype=np.uint8) * 255  # scale

            if self.crop_images:
                # find minimal bounding box and crop images (with fancy numpy slicing stuff)
                pixelpoints = cv2.findNonZero(mask)
                x, y, w, h = cv2.boundingRect(pixelpoints)
                color = color[y:y+h, x:x+w]
                depth = depth[y:y+h, x:x+w]
                mask = mask[y:y+h, x:x+w]
                pass

            # store images to file
            color_fn = f'color{i:05d}{ext:s}'
            depth_fn = f'depth{i:05d}{ext:s}'
            mask_fn = f'mask{i:05d}{ext:s}'

            cv2.imwrite(os.path.join(self.render_path, color_fn), color[..., ::-1])  # convert to BGR
            # cv2.imwrite(os.path.join(self.render_path, depth_fn), depth)  # need to adjust, depth is float in [m]
            cv2.imwrite(os.path.join(self.render_path, mask_fn), mask)

            # build ground truth file
            gt_image = {
                'id': i,
                'color_image': color_fn,
                # 'depth_image': depth_fn,
                'object_mask': mask_fn,
                'object_to_cam_pose': camera_pose.tolist()
            }
            gt_dict['image_labels'].append(gt_image)

            # at some point, we might want to have object masks and depth images as well.
            # some info on this: https://github.com/mmatl/pyrender/issues/15

            i += 1

        # save ground truth to file
        with open(gt_file, 'w') as file:
            json.dump(gt_dict, file, indent=2)  # use json.load to do the reverse

        print('finished generating renderings, stored files to', self.render_path)


def main():
    # let's do some unit testing here
    config = configparser.ConfigParser()
    config['General'] = {
        'renderings_directory': 'C:/please_delete_this/'
    }
    config['Renderer'] = {
        'model_dir': 'C:/Users/martin.rudorfer/docker-projects/pyrender-generator/models/',
        'model': 'iron.ply',
        # 'camera_pose_generator': 'RandomCameraPoseGenerator'
        'camera_pose_generator': 'IcoSphereCameraPoseGenerator'
    }
    config['Renderer.RandomCameraPoseGenerator'] = {
        'cam_distance_min': '90',
        'cam_distance_max': '90',
        'cam_distance_step': '5',
        'number_of_poses': '162',
        'rand_seed': '10'
    }

    renderer = ModelViewRenderer(config)
    renderer.visualize_camera_positions()


if __name__ == '__main__':
    main()
